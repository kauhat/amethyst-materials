use amethyst::ecs::{Component, DenseVecStorage};

pub struct Rotating;

impl Component for Rotating {
    type Storage = DenseVecStorage<Self>;
}

use amethyst::core::{Time, Transform};
use amethyst::core::cgmath::Deg;
use amethyst::ecs::{Read, ReadStorage, WriteStorage, Join, System};

use data::Rotating;

pub struct RotateObjectsSystem;

impl<'a> System<'a> for RotateObjectsSystem {
    type SystemData = (
        WriteStorage<'a, Transform>,
        ReadStorage<'a, Rotating>,
        Read<'a, Time>,
    );

    fn run(&mut self, (mut transforms, rotating, time): Self::SystemData) {
        for (mut transform, _) in (&mut transforms, &rotating).join() {
            transform.yaw_local(Deg(-90.0 * time.delta_seconds()));
            transform.roll_local(Deg(-90.0 * time.delta_seconds()));
        }
    }
}

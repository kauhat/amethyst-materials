use amethyst::core::cgmath::{Deg, Matrix4};
use amethyst::ecs::{Read, ReadExpect, WriteStorage, Join, System, Resources};
use amethyst::renderer::{Camera, ScreenDimensions, Projection, WindowEvent};
use amethyst::shrev::{EventChannel, ReaderId};

pub struct CameraSystem {
    reader: Option<ReaderId<WindowEvent>>,
}

impl CameraSystem {
    pub fn new() -> CameraSystem {
        CameraSystem { reader: None }
    }
}

impl<'a> System<'a> for CameraSystem {
    type SystemData = (
        Read<'a, EventChannel<WindowEvent>>,
        ReadExpect<'a, ScreenDimensions>,
        WriteStorage<'a, Camera>,
    );

    fn setup(&mut self, res: &mut Resources) {
        use amethyst::core::specs::prelude::SystemData;

        Self::SystemData::setup(res);
        self.reader = Some(res.fetch_mut::<EventChannel<WindowEvent>>().register_reader());
    }

    fn run (&mut self, (channel, screen, mut cameras): Self::SystemData) {
        for c in (&mut cameras).join() {
            c.proj = Matrix4::from(Projection::perspective(screen.aspect_ratio(), Deg(90.0)));
        }

        for event in channel.read(self.reader.as_mut().unwrap()) {
            println!("Received an event: {:?}", event);
        }
    }
}

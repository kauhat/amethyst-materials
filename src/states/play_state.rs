use amethyst::assets::{AssetStorage,Loader};
use amethyst::core::cgmath::{Deg, Matrix4};
use amethyst::core::{Transform, GlobalTransform};
use amethyst::ecs::prelude::*;
use amethyst::input::{is_close_requested, is_key_down};
use amethyst::renderer::*;
use amethyst::{GameData, State, StateData, Trans};

use data::*;

pub struct PlayState;

impl PlayState {
    fn initialise_camera(&mut self, world: &mut World) {
        let transform =
            Matrix4::from_translation([0.0, 0.0, -2.5].into()) * Matrix4::from_angle_y(Deg(180.));

        let aspect = {
            let dimensions = world.read_resource::<ScreenDimensions>();
            let aspect = dimensions.aspect_ratio();

            (aspect)
        };

        world.create_entity()
            .with(Camera::from(Projection::perspective(aspect, Deg(90.0))))
            .with(GlobalTransform(transform.into()))
            .build();
    }

    fn initialise_object(&mut self, world: &mut World) {
        // Load mesh and material
        let (mesh, material) = {
            let loader = world.read_resource::<Loader>();

            // Mesh
            let mesh_storage = world.read_resource();

            let mesh: MeshHandle = loader.load_from_data(
                Shape::IcoSphere(Some(4)).generate::<Vec<PosNormTangTex>>(None),
                (),
                &mesh_storage,
            );

            // Textures
            let texture_storage = world.read_resource::<AssetStorage<Texture>>();

            let albedo = loader.load(
                "resources/textures/concrete_color.png",
                PngFormat,
                Default::default(),
                (),
                &texture_storage,
            );

            let normal = loader.load(
                "resources/textures/concrete_normal.png",
                PngFormat,
                Default::default(),
                (),
                &texture_storage,
            );

            let ambient_occlusion = loader.load(
                "resources/textures/concrete_occlusion.png",
                PngFormat,
                Default::default(),
                (),
                &texture_storage,
            );

            // Material
            let mat_defaults = world.read_resource::<MaterialDefaults>().0.clone();

            let material = Material {
                albedo,
                normal,
                ambient_occlusion,
                ..mat_defaults.clone()
            };

            (mesh, material)
        };

        // Add object to the world
        world.create_entity()
            .with(GlobalTransform::default())
            .with(Transform::default())
            .with(Rotating {})
            .with(mesh)
            .with(material)
            .build();
    }

    fn initialise_lights(&mut self, world: &mut World) {
        //
        let light1: Light = PointLight {
            intensity: 6.0,
            color: [0.8, 0.8, 0.8].into(),
            ..PointLight::default()
        }.into();

        let light1_transform =
            GlobalTransform(Matrix4::from_translation([6.0, 6.0, -6.0].into()).into());

        world
            .create_entity()
            .with(light1)
            .with(light1_transform)
            .build();

        //
        let light2: Light = PointLight {
            intensity: 5.0,
            color: [0.0, 0.3, 0.7].into(),
            ..PointLight::default()
        }.into();

        let light2_transform =
            GlobalTransform(Matrix4::from_translation([6.0, -6.0, -6.0].into()).into());

        world
            .create_entity()
            .with(light2)
            .with(light2_transform)
            .build();
    }
}

impl<'a, 'b> State<GameData<'a, 'b>> for PlayState {
    fn on_start(&mut self, data: StateData<GameData>) {
        let StateData { world, .. } = data;

        // I don't think we need this
        //world.register::<Rotating>();

        self.initialise_object(world);
        self.initialise_lights(world);
        self.initialise_camera(world);
    }

    fn update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>> {
        data.data.update(&data.world);
        Trans::None
    }

    fn handle_event(&mut self, _: StateData<GameData>, event: Event) -> Trans<GameData<'a, 'b>> {
        if is_close_requested(&event) || is_key_down(&event, VirtualKeyCode::Escape) {
            Trans::Quit
        } else {
            Trans::None
        }
    }
}

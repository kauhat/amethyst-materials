extern crate amethyst;

use amethyst::core::transform::TransformBundle;
use amethyst::prelude::*;
use amethyst::renderer::*;

use amethyst::ui::{DrawUi};
use amethyst::utils::fps_counter::{FPSCounterBundle};

mod data;
mod states;
mod systems;

pub use data::*;
pub use states::*;
pub use systems::*;

fn main() -> Result<(), amethyst::Error> {
    amethyst::start_logger(Default::default());

    let display_config_path = "./resources/display_config.ron";

    // Pipeline
    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawPbm::<PosNormTangTex>::new())
            .with_pass(DrawUi::new())
    );

    //
    let game_data = GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(FPSCounterBundle::default())?
        .with_bundle(
            RenderBundle::new(pipe, Some(DisplayConfig::load(&display_config_path)))
        )?
        .with(RotateObjectsSystem, "rotate_objects", &[])
        .with(CameraSystem::new(), "camera", &[]);

    let mut game = Application::build("./", PlayState)?
        .build(game_data)?;

    game.run();

    Ok(())
}

